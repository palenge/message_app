package Utils;

import java.sql.ResultSet;
import java.sql.*;


public class Connect {
    Connection con;


    //Metodo Read Generico para los SELECT
    public ResultSet read(String query){
        ResultSet rs = null;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://" + Global.DB_URL + "/" + Global.DB_SCHEMA, Global.DB_USER, Global.DB_PASS);

            //con=DriverManager.getConnection("jdbc:mysql://localhost:3306/message_app","root","353805");
            Statement stmt=con.createStatement();
            rs=stmt.executeQuery(query);
            //con.close();
        }catch(Exception e){ System.out.println(e);}
        return rs;
    }

    //funcion para  cerrar conexiones
    public void close()
    {
        try{
            con.close();
        }catch(Exception e){ System.out.println(e);}
    }


    //Metodo ejecutar generico para INSERT UPDATE O DELETE
    public boolean execute(String query) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con=DriverManager.getConnection("jdbc:mysql://" + Global.DB_URL + "/" + Global.DB_SCHEMA, Global.DB_USER, Global.DB_PASS);
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            con.close();

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return true;
    }
}