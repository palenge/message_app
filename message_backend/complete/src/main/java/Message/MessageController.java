package Message;

import java.util.List;
import java.util.Map;

import Models.message_mes;
import org.springframework.web.bind.annotation.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/message")
public class MessageController {

    //generador complejo de json para crear acorde a la clase message_mes
    private String jsonEncodeReturn(List<message_mes> list)
    {
        JSONObject obj = new JSONObject();
        JSONArray jlist = new JSONArray();
        for (int i = 0; i < list.size(); i++) //Se itera por el listado de los message_mes
        {
            JSONObject obja = new JSONObject(); //Se crea el json object donde se va a crear cada uno de los message_mes
            obja.put("mes_id", list.get(i).getMes_Id());
            obja.put("mes_value", list.get(i).getMes_value());
            jlist.add(obja);
        }
        obj.put("class","message_mes");
        obj.put("values", jlist); //Se agrega la lista de message_mes
        return obj.toJSONString();
    }

    //Clase creada para generar los json basicos de respuestas
    private String jsonSimpleReturn(boolean status)
    {
        JSONObject obj = new JSONObject();
        obj.put("class","message_mes");
        obj.put("status", status?"OK":"ERROR");
        return obj.toJSONString();
    }

    //Metodo de leer registro
    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}", produces = "application/json")
    public String read(@PathVariable("id") int id) {
        message_mes mm =  new message_mes();
        //mm.read();
        //cn.connect();
        return jsonEncodeReturn(mm.read(id));
    }

    //Metodo de Listar
    @RequestMapping(method = RequestMethod.GET, value = "/get", produces = "application/json")
    public String read() {
        message_mes mm =  new message_mes();
        //mm.read();
        //cn.connect();
        return jsonEncodeReturn(mm.read());
    }

    //Metodo de insert
    @RequestMapping(method = RequestMethod.POST, value = "/insert", produces = "application/json")
    public String insert(@RequestBody Map<String,String> body) {
        message_mes mm =  new message_mes();
        //mm.read();
        //cn.connect();
        return jsonSimpleReturn(mm.insert(body.get("mes_value")));
    }

    //Metodo de actualizacion del registro
    @RequestMapping(method = RequestMethod.POST, value = "/update", produces = "application/json")
    public String update(@RequestBody Map<String,String> body) {  //Se realiza un map que conteng alos parametros del body del post
        message_mes mm =  new message_mes();
        //mm.read();
        //cn.connect();
        return jsonSimpleReturn(mm.update(body.get("mes_id") ,body.get("mes_value")));
    }


    //Metodo pde eliminacion
    @RequestMapping(method = RequestMethod.GET, value = "/delete/{id}", produces = "application/json")
    public String delete(@PathVariable("id") int id) {
        message_mes mm =  new message_mes();
        //mm.read();
        //cn.connect();
        return jsonSimpleReturn(mm.delete(id));
    }
}
