package Models;


import Utils.Connect;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class message_mes {
    //REGION privados

    //Identificcaodr del mensaje
    private int mes_id;

    //Valor del Mensaje
    private String mes_value;

    //Listado de message
    private List<message_mes> list;
    //END REGION

    //REGION propiedades
    //Obtener el identificador del mensaje
    public long getMes_Id() {
        return mes_id;
    }

    //Obtener el valor del mensaje
    public String getMes_value() {
        return mes_value;
    }

    //Obtener el listado de mensajes
    public List<message_mes> getlist() {
        return list;
    }
    //ENDREGION

    //REGION Constructores

    //Consctructor completo
    public message_mes(int mes_id, String mes_value) {
        this.mes_id = mes_id;
        this.mes_value = mes_value;
    }

    //Constructor vacio
    public message_mes() {
    }



    //END REGION

    //Funcion utilizada para pasar el resultset a las clases de message_mes
    private void decode(ResultSet rs)
    {
        try
        {
            list = new ArrayList<message_mes>();
            message_mes aux = new message_mes();
            while(rs.next())
            {
                aux = new message_mes(rs.getInt("mes_id"), rs.getString("mes_value"));
                list.add(aux);
            }
        }
        catch (Exception e)
        {
            System.out.println("Error Decoding");
        }
    }


    //Lector de message mes utilizando un ID
    public List<message_mes> read(int mes_id)
    {
        Connect cn = new Connect();
        decode(cn.read(String.format("SELECT * FROM message_mes where mes_id = %d", mes_id)));
        cn.close();
        return list;
    }

    //lector de message_mes para traer todo le listado
    public List<message_mes> read()
    {
        Connect cn = new Connect();
        decode(cn.read(String.format("SELECT * FROM message_mes")));
        cn.close();
        return list;
    }


    //Insertar de message_mes (por cuestiones de tiempo se descarto al verificaicon de si existe y que retorne el ID generado(
    public boolean insert(String mes_value)
    {
        try {
            Connect cn = new Connect();
            return cn.execute(String.format("INSERT INTO message_mes (mes_value) VALUES ('%s')", mes_value));
            //return true;
        }catch(Exception e)
        {
            System.out.println("Error Insertando");
            return false;
        }

    }

    //Actualizacion de message_mes
    public boolean update(String mes_id, String mes_value)
    {
        try {
            Connect cn = new Connect();
            return cn.execute(String.format("UPDATE message_mes SET mes_value = '%s' WHERE mes_id = %s", mes_value, mes_id));
            //return true;
        }catch(Exception e)
        {
            System.out.println("Error Insertando");
            return false;
        }

    }


    //Delete de message_mes por ID
    public boolean delete(int mes_id)
    {
        try {
            Connect cn = new Connect();
            return cn.execute(String.format("DELETE FROM message_mes WHERE mes_id = %d", mes_id));
            //return true;
        }catch(Exception e)
        {
            System.out.println("Error Insertando");
            return false;
        }

    }
}
